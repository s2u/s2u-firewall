<?php
#################################
### S.2.U Firewall System by Mr.Won         ###
### Phiên bản 3.0 - 11/05/2016                ###
#################################
if(empty($_COOKIE['PHPSESSID'])){
	session_start();
	session_id();
} else {
	session_id($_COOKIE['PHPSESSID']);
	session_start();
}

header('Content-Type: text/html; charset=utf-8');

$ts = microtime();
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Asia/Saigon');
define('S2UFW_BASE',dirname(__FILE__));
include_once(S2UFW_BASE.'/fw_function.php');
///////////////////////////////////////////////////////////
$now=time();
if(extension_loaded( 'zlib' )){
	ob_start( 'ob_gzhandler' );
}

$m = $_GET['s2m'];
$url = $_GET['s2u'];

//print_r($_SERVER); exit;
//print_r($_REQUEST); exit;

$url_c = str_replace(' ', '%20', 'http://'.$_SERVER['HTTP_HOST'].$url);
if (filter_var($url_c, FILTER_VALIDATE_URL) === false){echo "Lỗi URL"; exit;}

if($_SERVER['REQUEST_URI']==""){
	$url_r = 'http://'.$_SERVER['HTTP_HOST'];
} else {
	$url_r = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

if($m=='POST'){$mt = $_POST;}
else {$mt = $_GET;} 

foreach($mt as $key => $val){
	if($key!='s2m'&&$key!='s2u'&&$key!='s2ip'&&$key!='s2s'){
		if(!is_string($val)){
			$dataQ[$key] = $val;
		} else {
			$dataQ[$key] = trim($val);
		}
	}
}
if (!empty($_FILES)) {
	foreach ($_FILES as $fid => $file) {
		$dataQ[$fid] = curl_file_create($file["tmp_name"], $file["type"], $file["name"]);
	}
}

//print_r($dataQ); exit;

if($m=='GET'&&$dataQ!=""){$url_q = $url_c.'?'.http_build_query($dataQ);}
else {$url_q = $url_r;}

setFWStatus($config['s2u_fw_active']);

/* $header = getHeader($url_q);
//print_r($header); exit;
if($header['http_code']!=200&&$header['http_code']!=''){
	http_response_code($header['http_code']);
	showHTML('<font size="90px" color="red"><b>'.$header['http_code'].'</b></font><br>Trang web bị lỗi!');exit;
} */

$ip = getipFW();

if($config['s2u_fw_ipw']!=""){
	if(strpos($config['s2u_fw_ipw'], $ip) != 0){
		header('Location: '.$url_r); exit;
	}
}

if($config['s2u_fw_country']!=""){
	include_once(S2UFW_BASE.'/fw_geoiploc.php');
	$country=getCountryFromIP($ip);
	if($country!=""){
		if(strpos($config['s2u_fw_country'], $country) != 0){
			showHTML('Deny from '.$country.' country!'); exit;
		}
	}
}

/* $options = array(
	'http' => array(
		'header'  => 'Cookie: check=live\r\n'.
						 'Referer: '.$url_r.
						 'Connection: close\r\n'.
						 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\r\n',
		'method'  => $m,
		'content' => $dataQ
	)
);
$context = stream_context_create($options); */

$context = $dataQ;

if($config['s2u_fw_cache']==1){
	$tc = URLCache(0, md5($url_r.'?'.$dataQ), $now, '200 OK');
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', $tc['Last']).' GMT');
	header('Expires: '.gmdate('D, d M Y H:i:s', $now+$config['s2u_fw_cache_timeout']).' GMT');
	header('Cache-Control: max-age='.$config['s2u_fw_cache_timeout'].', must-revalidate');
	header('Pragma: ');
	header('Accept-Encoding: gzip, deflate');
	
	if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
		if($now-$tc['Last']<$config['s2u_fw_cache_timeout']){
			header("HTTP/1.1 304 Not modified"); exit;
		} else {
			URLCache(1, md5($url_r.'?'.$dataQ), '', '200 OK');
		}
	}
}

if($config['s2u_fw_active']==1){
	if($config['s2u_fw_cache_remove']==1){
		$files = glob('cache/*');
		foreach($files as $file){
			if(is_file($file)){
				unlink($file);
			}
		}
	}
	
	$status = checkClient($ip, $now, $url_q);
	if($status != 'Live'){exit;}
	$te = microtime() - $ts;
	usleep($te + 5000);
	if($m=='POST'||$m=='GET'){
		$content = getContent($url_c, $url_q, $context, $m);
		//header('Content-length: '.strlen($content)); 
		print $content;
	} else {
		header('Location: '.$url_q);
	}
} else if($config['s2u_fw_active']==2){	
	$hash = md5($url_q.$dataQ);
	$te = URLCache(0, md5($url_q.$hash), $now);
	$fc = "s2uC_".$hash;
	$fc = preg_replace('/(\W+)/',"", $fc);
	$cache_file = 'cache/'.$fc;
	if(file_exists($url_q.$cache_file)) {
		if(($now-$te)<$config['s2u_fw_cache_clear']){
			unlink($cache_file);
		} else {
			header('Location: '.dirname($url_q).'/'.$fc); exit;
		}
	}
	$content = getContent($url_c, $url_q, $context, $m);
	$fp = fopen($cache_file, 'w');
	fwrite($fp, $content);
	fclose($fp);
	header('Location: '.$fc); exit;
}
if(extension_loaded( 'zlib' )){ob_end_flush();}
?>
