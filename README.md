# S2U Firewall System

### Hệ thống tường lửa cho website PHP

 Version 3.0.6 - 20/04/2016

 Author: Mr.Won (won.baria@gmail.com)

 Website: https://gitlab.com/s2u/s2u-firewall

![S2U Firewall System](http://i.imgur.com/irdOMzh.jpg)

---

>Hệ thống chống ddos hiệu quả dành cho website của bạn, khả năng chống botnet, flood, spam,...
>hạn chế tối đa từ các lượt tấn công! Nó chạy ngầm và phân tích tình hình, tùy biến cấu hình dễ dàng,
>tương thích với nhiều website sử dụng ngôn ngữ PHP.

---

#### Yêu cầu hệ thống (System request):
 - PHP 5.x
 - Hỗ trợ Mod_rewrite

---

#### Các tính năng:
 - Chống các dạng tấn công mạng (ddos, spam, flood,..).
 - Gọn nhẹ, cấu hình và quản lý trực quan.
 - Chạy ngầm, không ảnh hưởng nhiều tới website.
 - Hai chế độ tường lửa đơn giản và chuyên nghiệp.
 - Phân tích tình trạng và tự động chuyển đổi chế độ phòng chống ddos.
 - Ngăn chặn với 2 lớp bảo vệ.
 - Cảnh báo trang bị chèn iframe và chống bị chèn iframe. (nếu host hỗ trợ auto_append_file trong htaccess)
 - Thiết lập thời gian bảo trì trong ngày.
 - Thử cấu hình tưởng lửa bằng công cụ S.2.U Ddos Flash Client.
 - Chặn nguồn ddos flash (domain, ip host, ip bot).
 - Hỗ trợ nhiều dạng website.
 - Mở khóa IP bằng captcha.
 - Hỗ trợ chạy full site, không cần include bất kì file nào. (nếu host hỗ trợ auto_prepend_file trong htaccess)
 - Chặn máy trong mạng LAN bằng Cookies.
 - Chặn các IP từ các nước khác.
 - Chế độ đăng nhập trước khi truy cập site.
 - Thông báo tình trạng firewall qua email.
 - Nhiều cấu hình tự thiết lập dễ dàng.
 - Và một số tính năng khác bạn có thể tham khảo ở trong trang Quản Lý Hệ Thống.

---

#### Hướng dẫn cài đặt:
https://gitlab.com/s2u/s2u-firewall/wikis/Hướng-dẫn-Cài-đặt/
 
---

#### Hướng dẫn nâng cấp:
 Xóa hết thư mục cũ và up thư mục mới lên.

---

#### Liên hệ hoặc hỗ trợ:
https://gitlab.com/s2u/s2u-firewall/issues

Email: won.baria@gmail.com